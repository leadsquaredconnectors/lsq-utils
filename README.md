##LSQ Utility Node Module

### How to use this package
add lsqUtils as dependency in you project under package.json. Please make sure to have your git autentication configured for ssh key.
```json
{
"lsqUtils": "git+ssh://git@bitbucket.org/leadsquaredconnectors/lsqutils.git"
}
```
Refer https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html for more information.

Once added, run <code>npm install</code> to update dependencies. Currently, only single version of repository is maintained.