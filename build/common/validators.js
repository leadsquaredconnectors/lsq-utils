"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkRegex = exports.isValidFileName = exports.isValidEmail = void 0;
var EMAIL_VALIDATOR = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var FILE_NAME_VALIDATOR = /^[a-zA-Z0-9\s_\\.\-\(\):]{1,200}\.[a-zA-Z0-9]{1,10}$/;
var isValidEmail = function (email) {
    return EMAIL_VALIDATOR.test(email);
};
exports.isValidEmail = isValidEmail;
var isValidFileName = function (fileName) {
    return FILE_NAME_VALIDATOR.test(fileName);
};
exports.isValidFileName = isValidFileName;
var checkRegex = function (value, regex) {
    return regex.test(value);
};
exports.checkRegex = checkRegex;
