"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FileUtils = /** @class */ (function () {
    function FileUtils() {
    }
    FileUtils.validateFileName = function (fileName, fileType, allowedFileTypes) {
        var isAllowedFileType = allowedFileTypes.indexOf(fileType) >= 0;
        var regex = /^[a-zA-Z0-9\s_\\.\-\(\):]{1,200}\.[a-zA-Z0-9]{1,10}$/;
        var validFileName = regex.test(fileName);
        var isValidFileNameLength = fileName.length <= 255;
        return validFileName && isAllowedFileType && isValidFileNameLength;
    };
    return FileUtils;
}());
exports.default = FileUtils;
