export default class FileUtils {
    static validateFileName: (fileName: string, fileType: string, allowedFileTypes: string[]) => boolean;
}
