export default class StringUtils {
    static getFullName(firstName: string, lastName: string): string;
    static pluralize(count: number, word: string): string;
    static truncate(input: string, maxLength: number): string;
    static isDoubleByte: (message: string) => boolean;
    static stringify: (value: any) => string;
    static checkIfValue: (value: any) => any;
}
