import querystring from "query-string";
export default class UrlUtil {
    static appendParams(config?: any): void;
    static getQueryStringParams(): querystring.ParsedQuery<string>;
    static getQueryStringParamsWithHash(): querystring.ParsedQuery<string>;
    static getLogQueryStringParams(): querystring.ParsedQuery<string>;
}
