"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectsEqual = exports.getUniqueData = exports.sortByKey = exports.findObjInArray = exports.searchObjInArray = exports.mergeDeep = exports.isObject = exports.compareValues = void 0;
exports.compareValues = function (key, order) {
    if (order === void 0) { order = "asc"; }
    return function (a, b) {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }
        var varA = typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
        var varB = typeof b[key] === "string" ? b[key].toUpperCase() : b[key];
        var comparison = 0;
        if (varA > varB) {
            comparison = 1;
        }
        else if (varA < varB) {
            comparison = -1;
        }
        return order === "desc" ? comparison * -1 : comparison;
    };
};
/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
exports.isObject = function (item) {
    return item && typeof item === "object" && !Array.isArray(item);
};
/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
exports.mergeDeep = function (target) {
    var _a, _b;
    var sources = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        sources[_i - 1] = arguments[_i];
    }
    if (!sources.length)
        return target;
    var source = sources.shift();
    if (exports.isObject(target) && exports.isObject(source)) {
        for (var key in source) {
            if (exports.isObject(source[key])) {
                if (!target[key])
                    Object.assign(target, (_a = {}, _a[key] = {}, _a));
                exports.mergeDeep(target[key], source[key]);
            }
            else {
                Object.assign(target, (_b = {}, _b[key] = source[key], _b));
            }
        }
    }
    return exports.mergeDeep.apply(void 0, __spreadArrays([target], sources));
};
// used in zoom connector
exports.searchObjInArray = function (key, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].name === key) {
            return array[i];
        }
    }
};
exports.findObjInArray = function (searchKey, searchValue, array) {
    for (var index = 0; index < array.length; index++) {
        if (array[index][searchKey] === searchValue) {
            return index;
        }
    }
    return -1;
};
exports.sortByKey = function (array, key, order) {
    if (order === void 0) { order = "asc"; }
    return array
        ? array.sort(function (a, b) {
            var x = isNaN(a[key]) ? a[key].toLowerCase() : a[key];
            var y = isNaN(b[key]) ? b[key].toLowerCase() : b[key];
            return order === "desc"
                ? -1 * (x < y ? -1 : x > y ? 1 : 0)
                : x < y
                    ? -1
                    : x > y
                        ? 1
                        : 0;
        })
        : [];
};
exports.getUniqueData = function (data, uniqueIdentifierKey) {
    var uniqueIds = data
        .map(function (item) { return item[uniqueIdentifierKey]; })
        .filter(function (value, index, self) { return self.indexOf(value) === index; });
    var uniqueRecords = [];
    if (uniqueIds) {
        uniqueRecords = uniqueIds.map(function (id) {
            return __assign({}, data.find(function (obj) { return obj[uniqueIdentifierKey] === id; }));
        });
    }
    return uniqueRecords;
};
exports.objectsEqual = function (obj1, obj2) {
    return typeof obj1 === "object" && Object.keys(obj1).length > 0
        ? Object.keys(obj1).length === Object.keys(obj2).length &&
            Object.keys(obj1).every(function (prop) { return exports.objectsEqual(obj1[prop], obj2[prop]); })
        : obj1 === obj2;
};
