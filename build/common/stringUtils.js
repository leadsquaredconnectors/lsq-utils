"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StringUtils = /** @class */ (function () {
    function StringUtils() {
    }
    StringUtils.getFullName = function (firstName, lastName) {
        var name = [];
        if (firstName && firstName !== 'null') {
            name.push(firstName);
        }
        if (lastName && lastName != null) {
            name.push(lastName);
        }
        if (name.length === 0) {
            name.push('[No Name]');
        }
        return name.join(' ');
    };
    StringUtils.pluralize = function (count, word) {
        return count === 1 ? "1 " + word : count + " " + word + "s";
    };
    StringUtils.truncate = function (input, maxLength) {
        return input.length > maxLength ? input.substring(0, maxLength) + "..." : input;
    };
    ;
    StringUtils.isDoubleByte = function (message) {
        var gsm7bitUnits = 0;
        var gsm7bitChars = "@£$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";
        var gsm7bitExChar = "^{}\\[~]|€";
        for (var i = 0, len = message.length; i < len; i++) {
            if (gsm7bitUnits != null) {
                if (gsm7bitChars.indexOf(message.charAt(i)) > -1) {
                    gsm7bitUnits++;
                }
                else if (gsm7bitExChar.indexOf(message.charAt(i)) > -1) {
                    gsm7bitUnits += 2;
                }
                else {
                    gsm7bitUnits = null;
                }
            }
        }
        if (gsm7bitUnits === null) {
            return true;
        }
        return false;
    };
    StringUtils.stringify = function (value) {
        // eslint-disable-next-line
        return JSON.stringify(value).replace(/\"([^(\")"]+)\":/g, "$1:");
    };
    StringUtils.checkIfValue = function (value) {
        return (value === "null" || value === undefined) ? "" : value;
    };
    return StringUtils;
}());
exports.default = StringUtils;
