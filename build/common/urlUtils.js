"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var query_string_1 = __importDefault(require("query-string"));
var UrlUtil = /** @class */ (function () {
    function UrlUtil() {
    }
    UrlUtil.appendParams = function (config) {
        if (config === void 0) { config = {}; }
        var params = [];
        Object.keys(config.headers).forEach(function (key) {
            params.push(key.split("_")[1] + "=" + config.headers[key]);
        });
        //Append params to url
        var url = config.url;
        config.url = url + (url.indexOf("?") >= 0 ? "&" : "?") + params.join("&");
    };
    UrlUtil.getQueryStringParams = function () {
        return query_string_1.default.parse(unescape(window.location.search), { decode: false });
    };
    UrlUtil.getQueryStringParamsWithHash = function () {
        return query_string_1.default.parse(unescape(window.location.search + window.location.hash), {
            decode: false
        });
    };
    UrlUtil.getLogQueryStringParams = function () {
        var values = this.getQueryStringParams();
        Object.keys(values).map(function (q) {
            if (q.toLowerCase() === "accesskey" || q.toLowerCase() === "secretkey") {
                delete values[q];
            }
        });
        values.url = window.location.href.split("?")[0];
        values.browser = window.navigator.userAgent;
        return values;
    };
    return UrlUtil;
}());
exports.default = UrlUtil;
