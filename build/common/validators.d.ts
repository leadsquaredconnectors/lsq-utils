declare const isValidEmail: (email: string) => boolean;
declare const isValidFileName: (fileName: string) => boolean;
declare const checkRegex: (value: string, regex: any) => any;
export { isValidEmail, isValidFileName, checkRegex };
