export declare const compareValues: (key: string, order?: string) => (a: any, b: any) => number;
/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export declare const isObject: (item: any) => boolean;
/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
export declare const mergeDeep: (target: any, ...sources: any) => any;
export declare const searchObjInArray: (key: string, array: any) => any;
export declare const findObjInArray: (searchKey: string, searchValue: string, array: any) => number;
export declare const sortByKey: (array: any, key: string, order?: string) => any;
export declare const getUniqueData: (data: Array<any>, uniqueIdentifierKey: string) => any[];
export declare const objectsEqual: (obj1: any, obj2: any) => boolean;
