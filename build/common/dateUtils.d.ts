import moment, { Moment } from "moment-timezone";
export default class DateUtils {
    static DefaultDateFormat: string;
    static getTimeZone: () => string;
    static format: (date: any, displayFormat?: string, timeZone?: string | undefined) => string;
    static getUTCDate: (date: any) => any;
    static getDate: (date: string) => moment.Moment;
    static getTimeIntervals: (startDateString?: string, endDateString?: string, interval?: number) => {
        key: string;
        value: string;
    }[];
    static getNextTimeInterval: (interval?: number, date?: string) => Date;
    static getDateInTimezone: (format: string, timezone?: string | undefined) => moment.Moment;
    static formatCurrentDateTime: (format: string) => string;
    static parseDateTime(date: string, time: string, dateFormat: string, timeFormat: string): moment.Moment;
    static parseUTCDateTime(date: string, time: string, dateFormat: string, timeFormat: string, timezone: string): moment.Moment;
    static dateDiff(parsedFromDate: Moment, parsedToDate: Moment, param?: any): number;
    static convertToDate: (date: any) => any;
    static getTimezoneDate: (date: any, timeZone?: string) => moment.Moment;
    static getDateRange: (range: string, timezone?: string, date?: any) => {
        startDate: moment.Moment;
        endDate: moment.Moment;
    } | {
        startDate: string;
        endDate: string;
    };
    static getDateTimeForESS: (date: string, dayStart: boolean | undefined, timezone: string) => string;
    static isMoment: (date: any) => boolean;
    static momentToDate: (date: any) => any;
    static rangeValues: () => {
        key: string;
        value: string;
    }[];
    static getTimezoneOffset: (timezone: string) => string;
}
