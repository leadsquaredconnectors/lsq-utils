"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var moment_timezone_1 = __importDefault(require("moment-timezone"));
var DateUtils = /** @class */ (function () {
    function DateUtils() {
    }
    DateUtils.parseDateTime = function (date, time, dateFormat, timeFormat) {
        return moment_timezone_1.default(date + " " + time, dateFormat + " " + timeFormat);
    };
    DateUtils.parseUTCDateTime = function (date, time, dateFormat, timeFormat, timezone) {
        var tzTime = moment_timezone_1.default(date + " " + time, dateFormat + " " + timeFormat);
        return tzTime.subtract(tzTime.utcOffset(), "minutes");
    };
    DateUtils.dateDiff = function (parsedFromDate, parsedToDate, param) {
        if (param === void 0) { param = "minutes"; }
        return parsedFromDate.diff(parsedToDate, param);
    };
    DateUtils.DefaultDateFormat = "YYYY-MM-DD hh:mm A";
    DateUtils.getTimeZone = function () {
        return Intl.DateTimeFormat().resolvedOptions().timeZone;
    };
    DateUtils.format = function (date, displayFormat, timeZone) {
        if (displayFormat === void 0) { displayFormat = "DD-MMM-YY | HH:mm"; }
        timeZone = timeZone ? timeZone : DateUtils.getTimeZone();
        return moment_timezone_1.default(DateUtils.convertToDate(date)).tz(timeZone).format(displayFormat);
    };
    DateUtils.getUTCDate = function (date) {
        return DateUtils.convertToDate(date).toUTCString();
    };
    DateUtils.getDate = function (date) {
        return moment_timezone_1.default(DateUtils.convertToDate(date));
    };
    DateUtils.getTimeIntervals = function (startDateString, endDateString, interval) {
        if (startDateString === void 0) { startDateString = "2000-1-1 12:00:00 AM"; }
        if (endDateString === void 0) { endDateString = "2000-1-1 11:59:59 PM"; }
        if (interval === void 0) { interval = 15; }
        var start = moment_timezone_1.default(startDateString, DateUtils.DefaultDateFormat);
        var end = moment_timezone_1.default(endDateString, DateUtils.DefaultDateFormat);
        start.minutes(Math.ceil(start.minutes() / interval) * interval);
        var result = [];
        var current = moment_timezone_1.default(start);
        while (current <= end) {
            result.push({
                key: current.format("HH:mm"),
                value: current.format("hh:mm A")
            });
            current.add(interval, "minutes");
        }
        return result;
    };
    DateUtils.getNextTimeInterval = function (interval, date) {
        if (interval === void 0) { interval = 15; }
        if (date === void 0) { date = DateUtils.format(Date.now(), DateUtils.DefaultDateFormat); }
        var start = moment_timezone_1.default(date, DateUtils.DefaultDateFormat);
        start.minutes(Math.ceil(start.minutes() / 15) * 15);
        if (interval !== 15) {
            start.minutes(start.minutes() + interval);
        }
        return moment_timezone_1.default(start).toDate();
    };
    DateUtils.getDateInTimezone = function (format, timezone) {
        return moment_timezone_1.default(format);
    };
    DateUtils.formatCurrentDateTime = function (format) {
        return DateUtils.format(new Date(), format);
    };
    DateUtils.convertToDate = function (date) {
        return date ? (typeof date.getMonth === "function" ? date : new Date(date)) : date;
    };
    DateUtils.getTimezoneDate = function (date, timeZone) {
        if (timeZone === void 0) { timeZone = DateUtils.getTimeZone(); }
        return moment_timezone_1.default(DateUtils.convertToDate(date)).tz(timeZone);
    };
    DateUtils.getDateRange = function (range, timezone, date) {
        if (timezone === void 0) { timezone = DateUtils.getTimeZone(); }
        if (date === void 0) { date = new Date(); }
        // Dont do --> let dateObj: any = let DateUtils.getTimezoneDate(new Date(), timezone)
        // as the subtract will change the dateObj and the same refrence will be updated on every change
        switch (range) {
            case "today": {
                return {
                    startDate: DateUtils.getTimezoneDate(date, timezone),
                    endDate: DateUtils.getTimezoneDate(date, timezone)
                };
            }
            case "yesterday": {
                return {
                    startDate: DateUtils.getTimezoneDate(date, timezone).subtract(1, "day"),
                    endDate: DateUtils.getTimezoneDate(date, timezone).subtract(1, "day")
                };
            }
            case "last7Days": {
                return {
                    startDate: DateUtils.getTimezoneDate(date, timezone).subtract(6, "days"),
                    endDate: DateUtils.getTimezoneDate(date, timezone)
                };
            }
            case "last30Days": {
                return {
                    startDate: DateUtils.getTimezoneDate(date, timezone).subtract(29, "days"),
                    endDate: DateUtils.getTimezoneDate(date, timezone)
                };
            }
            case "customDate": {
                return {
                    startDate: DateUtils.getTimezoneDate(date, timezone),
                    endDate: DateUtils.getTimezoneDate(date, timezone)
                };
            }
            default: {
                var dateObj = DateUtils.getTimezoneDate(date, timezone);
                return {
                    startDate: dateObj.format("YYYY-MM-DDT00:00:00Z"),
                    endDate: dateObj.format("YYYY-MM-DDT23:59:59Z")
                };
            }
        }
    };
    DateUtils.getDateTimeForESS = function (date, dayStart, timezone) {
        if (dayStart === void 0) { dayStart = true; }
        var timeZoneDate = moment_timezone_1.default(date);
        var timeZoneOffset = moment_timezone_1.default(date).tz(timezone).format("Z");
        return dayStart
            ? timeZoneDate.format("YYYY-MM-DDT00:00:00") + timeZoneOffset
            : timeZoneDate.format("YYYY-MM-DDT23:59:59") + timeZoneOffset;
    };
    DateUtils.isMoment = function (date) {
        return moment_timezone_1.default.isMoment(date);
    };
    DateUtils.momentToDate = function (date) {
        return moment_timezone_1.default.isMoment(date) ? date.toDate() : date;
    };
    DateUtils.rangeValues = function () {
        return [
            {
                key: "today",
                value: "Today"
            },
            {
                key: "yesterday",
                value: "Yesterday"
            },
            {
                key: "last7Days",
                value: "Last 7 Days"
            },
            {
                key: "last30Days",
                value: "Last 30 Days"
            },
            {
                key: "customDate",
                value: "Custom Date"
            }
        ];
    };
    DateUtils.getTimezoneOffset = function (timezone) {
        return moment_timezone_1.default.tz(timezone).format("Z");
    };
    return DateUtils;
}());
exports.default = DateUtils;
