export default class CacheApiClient {
    private axiosClient;
    constructor(credentials: {
        baseUrl: string;
        authToken: string;
    });
    private getResponse;
    addHashset: (args: object, keyName: string) => Promise<unknown>;
    getHashset: (keyName: string) => Promise<unknown>;
    deleteHashset: (keyName: string, fieldName: string) => Promise<unknown>;
    getHashsetWithField: (keyName: string, fieldName: string) => Promise<unknown>;
}
