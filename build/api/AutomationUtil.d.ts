export default class AutomationBaseClient {
    GetDataFromRequest: (eventType: string, mainEventType: string, data: any) => any;
    GetOpportunityLeadModel: (data: any) => {
        LeadId: any;
        OpportunityData: any;
        ActivityData: any;
        LeadData?: undefined;
    } | {
        LeadData: any;
        OpportunityData: any;
        ActivityData: any;
        LeadId?: undefined;
    } | undefined;
    GetOpportunityActivityLeadModel: (data: any) => {
        LeadData: any;
        OpportunityData: any;
        ActivityData: any;
    };
    GetLeadCreateLeadModel: (data: any) => {
        LeadData: any;
    };
    GetLeadUpdateLeadModel: (data: any) => {
        LeadData: any;
    };
    GetActivityCreateLeadModel: (data: any) => {
        LeadData: any;
        ActivityData: any;
    };
    GetActivityUpdateLeadModel: (data: any) => {
        LeadId: any;
        ActivityData: any;
    };
    GetLeadAddedToListLeadModel: (data: any) => {
        LeadId: any;
    };
    IsActivityAutomation: (mainEventType: string) => boolean;
    formatActivityDataOnSubAutomation: (data: any) => any;
    IsTaskAutomation: (mainEventType: string) => boolean;
    GetSubAutomationLeadModel: (data: any, mainEventType: string) => any;
    GetTaskLeadModel: (data: any) => {
        LeadId: any;
        TaskData: any;
    };
    GetTaskCompleteLeadModel: (data: any) => {
        LeadId: any;
        TaskData: any;
    };
    HasLatestData: (jsonData: any) => any;
    private mapActivityFieldsToDict;
    populateTemplateKeyMapping: (Message: any, leadDetails: any, activityDetails: any, userDetails: any, taskDetails: any, linkDetails: any, activityOwnerDetails: any, leadOwnerDetails: any, taskOwnerDetails: any, opportunityOwnerDetails: any, opportunityDetails: any, otherDetails?: {
        timeZone?: string | undefined;
        dateTimeFormat?: string | undefined;
    } | undefined) => any;
    static mailMergeMessage: (details: any, Message: string, data: any, value: any, timeZone: string, taskFlag: any, dateTimeFormat: string) => string;
    static taskFieldMapping: () => any;
}
