export declare enum AuthTokenSource {
    graphql = 0,
    restUrlWithBody = 1,
    restUrlWithQueryString = 2
}
