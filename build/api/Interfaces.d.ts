export interface AuthData {
    UserId: string;
    OrgShortCode: number;
    Role: string;
    ApiUrl?: string;
    AppUrl?: string;
    Timezone?: string;
}
export interface CustomActionUpdateData {
    Id: string;
    Title: string;
    URL: string;
    Method: string;
    ShowInWeb: boolean;
    ShowInMobile: boolean;
    RestrictedRoles: Array<String>;
    Data: string;
    ActionType: string;
    ActionInvocationType: string;
}
export interface UserNotifyData {
    EmailContent: {
        From: string;
        Subject: string;
        ContentHTML: string;
    };
    UserEmailAddresses?: Array<String>;
    Roles?: Array<String>;
}
