export interface V2Response {
    status: "Error" | "Success";
    data: any;
}
export default class ApiV2AsyncClient {
    private axiosClient;
    private credentials;
    private lastInvocationTime;
    constructor(credentials: {
        baseUrl: string;
        authToken: string;
        accessKey: string;
        secretKey: string;
    }, delay?: number);
    private getResponse;
    asyncCreateActivity: (args: any) => Promise<V2Response>;
}
