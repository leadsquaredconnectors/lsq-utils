"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthTokenSource = void 0;
var AuthTokenSource;
(function (AuthTokenSource) {
    AuthTokenSource[AuthTokenSource["graphql"] = 0] = "graphql";
    AuthTokenSource[AuthTokenSource["restUrlWithBody"] = 1] = "restUrlWithBody";
    AuthTokenSource[AuthTokenSource["restUrlWithQueryString"] = 2] = "restUrlWithQueryString";
})(AuthTokenSource = exports.AuthTokenSource || (exports.AuthTokenSource = {}));
