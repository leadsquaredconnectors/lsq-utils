export default class ConnectorApiClient {
    private axiosClient;
    constructor(credentials: {
        baseUrl: string;
        authToken: string;
    });
    private getResponse;
    bulkMessageHandler: (args: {
        accessKey: string;
        secretKey: string;
        providerId: number;
        metaDataKey: string;
        listId: string;
        apiPageSize: number;
        connectorRecord: any;
        leadIds: string;
        eventName: string;
    }) => Promise<unknown>;
    bulkAttachmentsHandler: (args: {
        prospectActivityId: string;
        attachmentName: string;
        description: string;
        attachmentFileUrl: string;
        accessKey: string;
        secretKey: string;
    }[]) => Promise<unknown>;
}
