export default class ApiResponseUtil {
    static transformObjectArray: (objArray: Object[], itemKeProperty: string, itemValueProperty: string) => {
        key: any;
        value: any;
    }[];
    static transformObjectCollection: (list: any, itemKeProperty: string, itemValueProperty: string) => any;
    static getLeadFieldsByType: (list: any, type: string, leadObject?: any) => any;
    static getReplacedValue: (property: any, obj: any, key: string) => any;
    static getMailMergeFields: (objectList: any, format: string, type: string, ...objects: any) => any;
}
