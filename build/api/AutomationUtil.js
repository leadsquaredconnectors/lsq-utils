"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var momentTimezone = __importStar(require("moment-timezone"));
var AutomationBaseClient = /** @class */ (function () {
    function AutomationBaseClient() {
        var _this = this;
        this.GetDataFromRequest = function (eventType, mainEventType, data) {
            var LeadData;
            switch (eventType) {
                case "Lead_Post_Create":
                case "OnSpecificDate":
                case "CronLead":
                    LeadData = _this.GetLeadCreateLeadModel(data);
                    break;
                case "Lead_Post_Update":
                    LeadData = _this.GetLeadUpdateLeadModel(data);
                    break;
                case "Activity_Post_Create":
                    LeadData = _this.GetActivityCreateLeadModel(data);
                    break;
                case "Activity_Post_Update":
                    LeadData = _this.GetActivityUpdateLeadModel(data);
                    break;
                case "LeadAddedToList":
                    LeadData = _this.GetLeadAddedToListLeadModel(data);
                    break;
                case "SubAutomation":
                    LeadData = _this.GetSubAutomationLeadModel(data, mainEventType);
                    break;
                case "Task_Create":
                case "Task_Reminder":
                    LeadData = _this.GetTaskLeadModel(data);
                    break;
                case "Task_Complete":
                    LeadData = _this.GetTaskCompleteLeadModel(data);
                    break;
                case "Opportunity_Post_Create":
                case "Opportunity_Post_Update":
                    LeadData = _this.GetOpportunityLeadModel(data);
                    break;
                case "Opportunity_Activity_Post_Create":
                case "Opportunity_Activity_Post_Update":
                    LeadData = _this.GetOpportunityActivityLeadModel(data);
                    break;
                default:
                    break;
            }
            return LeadData;
        };
        this.GetOpportunityLeadModel = function (data) {
            if (data.After) {
                return {
                    LeadId: data["After"]["RelatedProspectId"],
                    OpportunityData: data["After"],
                    ActivityData: data["After"]
                };
            }
            else if (data.Current) {
                var leadData = JSON.parse(JSON.stringify(data["Current"]));
                delete data["Current"];
                return {
                    LeadData: leadData,
                    OpportunityData: data,
                    ActivityData: data
                };
            }
        };
        this.GetOpportunityActivityLeadModel = function (data) {
            return {
                LeadData: data,
                OpportunityData: data,
                ActivityData: data
            };
        };
        this.GetLeadCreateLeadModel = function (data) {
            var leadData = _this.HasLatestData(data) ? data["Current"] : data;
            return { LeadData: leadData };
        };
        this.GetLeadUpdateLeadModel = function (data) {
            var leadData = _this.HasLatestData(data) ? data["Current"] : data["After"];
            return { LeadData: leadData };
        };
        this.GetActivityCreateLeadModel = function (data) {
            var leadData = _this.HasLatestData(data) ? data["Current"] : data;
            var ActivityData = data;
            return { LeadData: leadData, ActivityData: ActivityData };
        };
        this.GetActivityUpdateLeadModel = function (data) {
            var ActivityData = data["After"];
            var leadId = data["After"]["RelatedProspectId"];
            return { LeadId: leadId, ActivityData: ActivityData };
        };
        this.GetLeadAddedToListLeadModel = function (data) {
            var leadId = data["LeadIds"][0];
            return { LeadId: leadId };
        };
        this.IsActivityAutomation = function (mainEventType) {
            var Flag = false;
            if (!mainEventType.trim()) {
                throw "Main event type is missing.";
            }
            switch (mainEventType.trim().toLowerCase()) {
                case "activityautomation":
                case "onactivityadded":
                case "onactivityupdated":
                    Flag = true;
                    break;
            }
            return Flag;
        };
        this.formatActivityDataOnSubAutomation = function (data) {
            var result;
            data;
            var inboundData = JSON.parse(JSON.stringify(data));
            var leadData;
            if (data.Current) {
                leadData = data.Current;
                delete inboundData["Current"];
                result = {
                    LeadData: leadData,
                    ActivityData: inboundData
                };
            }
            else {
                result = {
                    LeadId: inboundData.ProspectID,
                    ActivityData: inboundData
                };
            }
            return result;
        };
        this.IsTaskAutomation = function (mainEventType) {
            var Flag = false;
            if (!mainEventType.trim()) {
                throw "Main event type is missing.";
            }
            switch (mainEventType.trim().toLowerCase()) {
                case "taskcreate":
                case "taskreminder":
                case "taskcomplete":
                    Flag = true;
                    break;
            }
            return Flag;
        };
        this.GetSubAutomationLeadModel = function (data, mainEventType) {
            var leadModel;
            try {
                if (data == null)
                    return leadModel;
                if (_this.IsActivityAutomation(mainEventType)) {
                    leadModel =
                        data["After"] != null
                            ? _this.formatActivityDataOnSubAutomation(data["After"])
                            : _this.formatActivityDataOnSubAutomation(data);
                }
                else if (_this.IsTaskAutomation(mainEventType)) {
                    leadModel = _this.GetTaskLeadModel(data);
                }
                else if (mainEventType === "LeadAddedToList") {
                    leadModel = _this.GetLeadAddedToListLeadModel(data);
                } // is activity automation
                else {
                    leadModel = {
                        LeadData: _this.HasLatestData(data)
                            ? data["Current"]
                            : data["After"] != null
                                ? data["After"]
                                : data
                    };
                }
            }
            catch (error) {
                console.error(error);
            }
            return leadModel;
        };
        this.GetTaskLeadModel = function (data) {
            var RequestData = {
                LeadId: data["LeadId"],
                TaskData: data
            };
            return RequestData;
        };
        this.GetTaskCompleteLeadModel = function (data) {
            var RequestData = { LeadId: data["LeadId"], TaskData: data };
            return RequestData;
        };
        this.HasLatestData = function (jsonData) {
            return jsonData && jsonData["Current"];
        };
        this.mapActivityFieldsToDict = function (fields, fieldDetails) {
            var result = {};
            fields.map(function (x) {
                result[x.SchemaName] = x.Value;
            });
            var field = JSON.parse(JSON.stringify(fieldDetails));
            delete field["Fields"];
            for (var i = 0; i < Object.keys(field).length; i++) {
                if (!result[Object.keys(field)[i]]) {
                    result[Object.keys(field)[i]] = field[Object.keys(field)[i]];
                }
            }
            return result;
        };
        this.populateTemplateKeyMapping = function (Message, leadDetails, activityDetails, userDetails, taskDetails, linkDetails, activityOwnerDetails, leadOwnerDetails, taskOwnerDetails, opportunityOwnerDetails, opportunityDetails, otherDetails) {
            if (!Message) {
                return Message;
            }
            var activityData = activityDetails && activityDetails.Data
                ? activityDetails.Data
                : activityDetails && activityDetails.Fields
                    ? _this.mapActivityFieldsToDict(activityDetails.Fields, activityDetails)
                    : activityDetails;
            var opportunityData;
            if (activityDetails &&
                opportunityDetails &&
                JSON.stringify(activityDetails) === JSON.stringify(opportunityDetails)) {
                opportunityData = activityData;
            }
            else {
                opportunityData =
                    opportunityDetails && opportunityDetails.Data
                        ? opportunityDetails.Data
                        : opportunityDetails && opportunityDetails.Fields
                            ? _this.mapActivityFieldsToDict(opportunityDetails.Fields, opportunityDetails)
                            : opportunityDetails;
            }
            var timeZone = userDetails && userDetails.TimeZone ? userDetails.TimeZone : "";
            if (!timeZone && (otherDetails === null || otherDetails === void 0 ? void 0 : otherDetails.timeZone)) {
                timeZone = otherDetails.timeZone;
            }
            var dateTimeFormat = (otherDetails === null || otherDetails === void 0 ? void 0 : otherDetails.dateTimeFormat) || "DD MMM YYYY hh:mm:ss A";
            while (Message.match(/@{(.*?),}/) !== null) {
                var value = Message.match(/@{(.*?),}/)[1];
                var data = value.split(":");
                var entity = data[0].toLowerCase().trim();
                switch (entity) {
                    case entity.startsWith("activity_"):
                        if (parseInt(entity.split("_")[1]) === activityDetails["ActivityEvent"]) {
                            Message = AutomationBaseClient.mailMergeMessage(activityData, Message, data, value, timeZone, false, dateTimeFormat);
                        }
                        break;
                    case entity.startsWith("opportunity_"):
                        if (parseInt(entity.split("_")[1]) === opportunityDetails["ActivityEvent"] ||
                            parseInt(entity.split("_")[1]) === opportunityDetails.OpportunityEvent) {
                            Message = AutomationBaseClient.mailMergeMessage(opportunityData, Message, data, value, timeZone, false, dateTimeFormat);
                        }
                        break;
                    case entity.startsWith("task_"):
                        if (parseInt(entity.split("_")[1]) === taskDetails.Name) {
                            Message = AutomationBaseClient.mailMergeMessage(taskDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        }
                        break;
                    case "lead":
                        Message = AutomationBaseClient.mailMergeMessage(leadDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "user":
                        Message = AutomationBaseClient.mailMergeMessage(userDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "leadowner":
                        Message = AutomationBaseClient.mailMergeMessage(leadOwnerDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "taskowner":
                        Message = AutomationBaseClient.mailMergeMessage(taskOwnerDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "activityowner":
                        Message = AutomationBaseClient.mailMergeMessage(activityOwnerDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "opportunityowner":
                        Message = AutomationBaseClient.mailMergeMessage(opportunityOwnerDetails, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "activity":
                        Message = AutomationBaseClient.mailMergeMessage(activityData, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "opportunity":
                        Message = AutomationBaseClient.mailMergeMessage(opportunityData, Message, data, value, timeZone, false, dateTimeFormat);
                        break;
                    case "task":
                        Message = AutomationBaseClient.mailMergeMessage(taskDetails, Message, data, value, timeZone, data[data.length - 1] ? false : true, dateTimeFormat);
                        break;
                    default:
                        Message = AutomationBaseClient.mailMergeMessage(null, Message, data, value, timeZone, false, dateTimeFormat);
                }
            }
            return Message;
        };
    }
    AutomationBaseClient.mailMergeMessage = function (details, Message, data, value, timeZone, taskFlag, dateTimeFormat) {
        var startIndex = Message.indexOf("@{");
        var endIndex = startIndex + value.length + 4;
        var fieldSchemaName;
        if (taskFlag) {
            var taskFieldDataMapping = AutomationBaseClient.taskFieldMapping();
            fieldSchemaName = taskFieldDataMapping[data[data.length - 1]]
                ? taskFieldDataMapping[data[data.length - 1]]
                : data[data.length - 1];
        }
        else {
            fieldSchemaName = data[data.length - 1];
        }
        if (details && details[fieldSchemaName]) {
            var fieldValue = details[fieldSchemaName];
            var replaceData = timeZone
                ? typeof fieldValue !== "number" &&
                    !isNaN(parseInt(fieldValue)) &&
                    !isNaN(Date.parse(fieldValue))
                    ? momentTimezone.tz(fieldValue + " Z", timeZone).format(dateTimeFormat)
                    : fieldValue
                : fieldValue;
            Message =
                Message.substr(0, startIndex) +
                    replaceData +
                    Message.substr(endIndex, Message.length);
        }
        else if (details && !details[fieldSchemaName]) {
            Message = Message.substr(0, startIndex) + Message.substr(endIndex, Message.length);
        }
        else {
            Message = Message.substr(0, startIndex) + Message.substr(endIndex, Message.length);
        }
        return Message;
    };
    AutomationBaseClient.taskFieldMapping = function () {
        var taskMapping = {
            Name: "TaskName",
            TaskType: "TaskType",
            TaskTypeConfiguration: "TaskTypeConfiguration",
            Description: "TaskDescription",
            RelatedEntityId: "RelatedEntityId",
            DueDate: "DueDateUTCTime",
            Reminder: "ReminderInMinutes",
            OwnerId: "OwnerId",
            CreatedBy: "CreatedBy",
            CreatedOn: "CreatedOn",
            EndDate: "EndDateUTCTime",
            Location: "Location",
            PercentCompleted: "PercentCompleted",
            Priority: "Priority",
            mx_Custom_1: "mx_Custom_1",
            mx_Custom_2: "mx_Custom_2",
            mx_Custom_3: "mx_Custom_3",
            mx_Custom_4: "mx_Custom_4",
            mx_Custom_5: "mx_Custom_5",
            mx_Custom_6: "mx_Custom_6",
            mx_Custom_7: "mx_Custom_7",
            mx_Custom_8: "mx_Custom_8",
            mx_Custom_9: "mx_Custom_9",
            mx_Custom_10: "mx_Custom_10",
            mx_Custom_11: "mx_Custom_11",
            mx_Custom_12: "mx_Custom_12",
            mx_Custom_13: "mx_Custom_13",
            mx_Custom_14: "mx_Custom_14",
            mx_Custom_15: "mx_Custom_15",
            mx_Custom_16: "mx_Custom_16",
            mx_Custom_17: "mx_Custom_17",
            mx_Custom_18: "mx_Custom_18",
            mx_Custom_19: "mx_Custom_19",
            mx_Custom_20: "mx_Custom_20"
        };
        return taskMapping;
    };
    return AutomationBaseClient;
}());
exports.default = AutomationBaseClient;
