import { AuthTokenSource } from "./AuthTokenSource";
export default class AuthTokenBase {
    static retrieveCredentialsKeys: (event: any, type: AuthTokenSource, accessVariableName?: string, secretVariableName?: string) => any;
    static getEncryptedToken: (authObj: any, accessKey: string, secretKey: string) => string;
    static getDecryptedToken: (authToken: string, accessKey: string, secretKey: string) => any;
}
