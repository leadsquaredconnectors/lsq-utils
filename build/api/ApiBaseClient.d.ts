import { AxiosInstance } from "axios";
import Logger from "../logger";
export default abstract class ApiBaseClient {
    private baseUrl;
    private logger;
    private axiosClient;
    static API_TOKEN_HEADER: string;
    private errorResponseHandler;
    abstract getBaseUrl(): string;
    static checkIfLocalStorageEnabled: () => boolean;
    constructor(params?: any, baseUrl?: string);
    protected callApi(): AxiosInstance;
    /**
     *
     * @param url received from v2/ call to find the request host
     */
    static getUrlWithProtocol(url: string): string;
    getLogger(): Logger;
    protected makeGraphRequest: (url: string, query: string) => Promise<unknown>;
}
