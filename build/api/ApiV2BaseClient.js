"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var ApiV2BaseClient = /** @class */ (function () {
    function ApiV2BaseClient(credentials, authData) {
        var _this = this;
        this.skipTransformError = false;
        this.timeout = 0; //ms
        this.setSkipTransformError = function (skipTransformError) {
            _this.skipTransformError = skipTransformError;
        };
        this.setTimeout = function (timeout) {
            _this.timeout = timeout;
        };
        this.getApiUrl = function (callback) { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (_this.authData.OrgShortCode === 0) {
                            _this.axiosClient
                                .get("/v2/Authentication.svc/UserByAccessKey.Get")
                                .then(function (data) {
                                var apiUrl = "https://" + data.data.LSQCommonServiceURLs.api;
                                var appUrl = "https://" + data.data.LSQCommonServiceURLs.app;
                                _this.authData = {
                                    UserId: data.data.Id,
                                    Role: data.data.Role,
                                    OrgShortCode: parseInt(data.data.OrgShortCode),
                                    ApiUrl: apiUrl,
                                    AppUrl: appUrl,
                                    Timezone: data.data.TimeZone
                                };
                                if (_this.timeout) {
                                    _this.axiosClient.defaults.timeout = _this.timeout;
                                }
                                _this.axiosClient.defaults.baseURL = apiUrl;
                                if (callback) {
                                    callback()
                                        .then(function (data) {
                                        resolve({
                                            statusCode: 200,
                                            Message: data.data
                                        });
                                    })
                                        .catch(function (err) {
                                        resolve({ statusCode: 500, Message: err });
                                    });
                                }
                                else {
                                    //resolve auth data
                                    resolve({
                                        statusCode: 200,
                                        Message: _this.authData
                                    });
                                }
                            })
                                .catch(function (err) {
                                _this.skipTransformError
                                    ? resolve({ statusCode: 500, Message: err })
                                    : resolve({
                                        statusCode: err && err.response && err.response.status
                                            ? err.response.status
                                            : 500,
                                        Message: err && err.response && err.response.data
                                            ? err.response.data
                                            : err
                                    });
                            });
                        }
                        else {
                            if (callback) {
                                callback()
                                    .then(function (data) {
                                    resolve({ statusCode: 200, Message: data.data });
                                })
                                    .catch(function (err) {
                                    _this.skipTransformError
                                        ? resolve({ statusCode: 500, Message: err })
                                        : resolve({
                                            statusCode: err && err.response && err.response.status
                                                ? err.response.status
                                                : 500,
                                            Message: err && err.response && err.response.data
                                                ? err.response.data
                                                : err
                                        });
                                });
                            }
                            else {
                                resolve({ statusCode: 200, Message: _this.authData });
                            }
                        }
                    })];
            });
        }); };
        this.getV2Response = function (request) {
            return new Promise(function (resolve, reject) {
                _this.getApiUrl(function () {
                    return _this.axiosClient(request);
                })
                    .then(function (data) {
                    resolve(data);
                })
                    .catch(function (err) {
                    resolve(err);
                });
            });
        };
        this.getAuthData = function () { return __awaiter(_this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.authData.OrgShortCode) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getApiUrl()];
                    case 1:
                        data = _a.sent();
                        return [2 /*return*/, data];
                    case 2: return [2 /*return*/, {
                            statusCode: 200,
                            Message: this.authData
                        }];
                }
            });
        }); };
        this.markTaskCompleted = function (id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Task.svc/MarkComplete",
                        params: {
                            id: id
                        }
                    })];
            });
        }); };
        this.getActivityDetailsById = function (id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/GetActivityDetails",
                        params: {
                            activityId: id
                        }
                    })];
            });
        }); };
        this.getUserOptions = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/UserManagement.svc/User/Attribute/Retrieve",
                        method: "post"
                    })];
            });
        }); };
        this.getTaskTypes = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Task.svc/RetrieveTaskTypeName"
                    })];
            });
        }); };
        this.getLeadsOption = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/LeadsMetaData.Get?excludeOptionSets=1"
                    })];
            });
        }); };
        this.getLeadDetailsById = function (leadId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/Leads.GetById",
                        params: {
                            id: leadId
                        },
                        method: "get"
                    })];
            });
        }); };
        this.getActivityTypes = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/ActivityTypes.Get"
                    })];
            });
        }); };
        this.createCustomActivityType = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/CreateType?",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateCustomActivityType = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/CustomActivity/UpdateSetting",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.createActivityByAPI = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/Create",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateActionHooks = function (args, connectorId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Connector.svc/CustomAction/Update",
                        params: {
                            connectorId: connectorId
                        },
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateAutomationActionHooks = function (args, connectorId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "v2/Connector.svc/AutomationAction/Update",
                        params: {
                            connectorId: connectorId
                        },
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.getUserById = function (id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/UserManagement.svc/User/Retrieve/ByUserId",
                        params: {
                            userId: id ? id : this.authData.UserId
                        }
                    })];
            });
        }); };
        this.getCountOfLeadsInList = function (id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadSegmentation.svc/List/Retrieve/MemberCount",
                        params: {
                            listId: id
                        }
                    })];
            });
        }); };
        this.getTaskTypeDetails = function (columnValue) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Task.svc/RetrieveTaskType",
                        params: {
                            columnName: "taskname",
                            columnValue: columnValue
                        }
                    })];
            });
        }); };
        this.getUsers = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/UserManagement.svc/Users.Get"
                    })];
            });
        }); };
        this.userAdvancedSearch = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/UserManagement.svc/User/AdvancedSearch",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.createTask = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Task.svc/Create",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.getLeadSearchByParameter = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/Leads/Retrieve/BySearchParameter",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.getTaskById = function (id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Task.svc/Retrieve.GetById",
                        params: {
                            id: id
                        }
                    })];
            });
        }); };
        this.createMultipleActivities = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/Bulk/CustomActivity/Add/ByLeadId",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateMutipleActivities = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/Bulk/CustomActivity/Update",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateActivityDetails = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/CustomActivity/Update",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.addActivityAttachments = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/Attachment/Add",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.getUserActions = function (connectorId, actionType) {
            return _this.getV2Response({
                url: "/v2/Connector.svc/CustomAction/Retrieve",
                params: {
                    connectorId: connectorId,
                    actionType: actionType
                }
            });
        };
        this.addUpdateUserAction = function (connectorId, actionType, data) {
            return _this.getV2Response({
                url: (data.Id
                    ? "/v2/Connector.svc/CustomAction/Update"
                    : "/v2/Connector.svc/CustomAction/Create") +
                    "?connectorId=" +
                    connectorId,
                method: "post",
                data: data
            });
        };
        this.deleteUserAction = function (connectorId, actionType, tabId) {
            return _this.getV2Response({
                url: "/v2/Connector.svc/CustomAction/Delete",
                method: "get",
                params: {
                    tabId: tabId,
                    connectorId: connectorId,
                    actionType: actionType
                }
            });
        };
        this.getLeadFieldsMetadata = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/LeadsMetaData.Get?excludeOptionSets=1",
                        method: "get"
                    })];
            });
        }); };
        this.getLeadFieldMetadata = function (schemaName, excludeOptionSets) {
            if (excludeOptionSets === void 0) { excludeOptionSets = 1; }
            return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.getV2Response({
                            url: "/v2/LeadManagement.svc/LeadsMetaData.Get?excludeOptionSets=" + excludeOptionSets,
                            params: {
                                schemaName: schemaName
                            }
                        })];
                });
            });
        };
        this.captureLead = function (data) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/Lead.Capture",
                        method: "post",
                        data: data
                    })];
            });
        }); };
        this.getLeadById = function (id) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/Leads.GetById",
                        params: {
                            id: id
                        }
                    })];
            });
        }); };
        this.getLeadsByIds = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/Leads/Retrieve/ByIds",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.createActionHooks = function (args, connectorId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Connector.svc/CustomAction/Create",
                        params: {
                            connectorId: connectorId
                        },
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateApplicationMenu = function (args, connectorId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Connector.svc/CustomWebAppMenu/Update",
                        params: {
                            connectorId: connectorId
                        },
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.getActionHooks = function (connectorId, actionType) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Connector.svc/CustomAction/Retrieve",
                        params: {
                            connectorId: connectorId,
                            actionType: actionType
                        },
                        method: "get"
                    })];
            });
        }); };
        this.createCustomActivity = function (data) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/CreateCustom",
                        method: "post",
                        data: data
                    })];
            });
        }); };
        this.createWebhook = function (data) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Webhook.svc/Create",
                        method: "post",
                        data: {
                            Description: data.description,
                            URL: data.url,
                            WebhookEvent: data.event,
                            IsSpecificLandingPage: typeof data.IsSpecificLandingPage === "undefined"
                                ? false
                                : data.IsSpecificLandingPage,
                            NotifyOnFailure: typeof data.notifyOnFailure === "undefined" ? true : data.notifyOnFailure,
                            WebhookProperties: null,
                            CustomHeaders: data.headers ? null : data.headers,
                            Method: "POST",
                            ContentType: "application/json"
                        }
                    })];
            });
        }); };
        this.deleteWebHook = function (webhookId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Webhook.svc/Delete",
                        params: {
                            webhookId: webhookId
                        },
                        method: "get"
                    })];
            });
        }); };
        this.searchLeadsByCriteria = function (args) {
            return _this.getV2Response({
                url: "/v2/LeadManagement.svc/Leads.Get",
                data: args,
                method: "post"
            });
        };
        this.getLandingPage = function (args) {
            return _this.getV2Response({
                url: "/v2/LandingPage.svc/Retrieve",
                data: args,
                method: "post"
            });
        };
        this.activityAdvancedSearch = function (args) {
            return _this.getV2Response({
                url: "/v2/ProspectActivity.svc/Activity/Retrieve/BySearchParameter",
                data: args,
                method: "post"
            });
        };
        this.getLeadByEmailId = function (emailId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/Leads.GetByEmailaddress",
                        params: {
                            emailaddress: emailId
                        }
                    })];
            });
        }); };
        this.getOpportunityTypes = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/OpportunityManagement.svc/GetOpportunityTypes"
                    })];
            });
        }); };
        this.getOpportunityById = function (id, url) { return __awaiter(_this, void 0, void 0, function () {
            var params;
            return __generator(this, function (_a) {
                params = url
                    ? {
                        OpportunityId: id,
                        getFileURL: url
                    }
                    : {
                        OpportunityId: id
                    };
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/OpportunityManagement.svc/GetOpportunityDetails",
                        params: params
                    })];
            });
        }); };
        this.getOpportunityMetadataByEventCode = function (code) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/OpportunityManagement.svc/GetOpportunityTypeMetadata",
                        params: {
                            code: code
                        }
                    })];
            });
        }); };
        this.retrieveWebhook = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Webhook.svc/Retrieve",
                        data: args,
                        method: "post"
                    })];
            });
        }); };
        this.isOpportunityEnabled = function (orgShortCode) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "v2/OpportunityManagement.svc/IsOpportunityEnabled",
                        params: {
                            orgId: orgShortCode
                        }
                    })];
            });
        }); };
        this.getLeadByPhonenumber = function (phone) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/LeadManagement.svc/RetrieveLeadByPhoneNumber",
                        params: {
                            phone: phone
                        }
                    })];
            });
        }); };
        this.createWebhookByArgs = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Webhook.svc/Create",
                        method: "post",
                        data: args
                    })];
            });
        }); };
        this.updateCustomWebAppMenu = function (data, connectorId) {
            if (data === void 0) { data = {}; }
            return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.getV2Response({
                            url: "/v2/Connector.svc/CustomWebAppMenu/Update?connectorId=" + connectorId,
                            method: "post",
                            data: data
                        })];
                });
            });
        };
        this.updateCustomAction = function (data, connectorId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/Connector.svc/CustomAction/Update?connectorId=" + connectorId,
                        method: "post",
                        data: data
                    })];
            });
        }); };
        this.postSalesActivity = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/SalesActivity.svc/Create",
                        data: args,
                        method: "post"
                    })];
            });
        }); };
        this.getSalesActivityProducts = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/SalesActivity.svc/RetrieveProducts",
                        data: args,
                        method: "post"
                    })];
            });
        }); };
        this.getActivitySetting = function (activityId) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/ProspectActivity.svc/CustomActivity/GetActivitySetting",
                        params: {
                            code: activityId
                        }
                    })];
            });
        }); };
        this.getUserByEmailAddress = function (email) {
            return _this.getV2Response({
                url: "/v2/UserManagement.svc/User/Retrieve/ByEmailAddress",
                params: {
                    emailAddress: email
                }
            });
        };
        this.getAllList = function () {
            return _this.getV2Response({
                url: "/v2/LeadManagement.svc/Lists.Get"
            });
        };
        this.addLeadToStaticList = function (listId, leadId) {
            return _this.getV2Response({
                url: "/v2/LeadManagement.svc/AddLeadToStaticList",
                params: {
                    listId: listId,
                    leadId: leadId
                },
                method: "post"
            });
        };
        this.notifyUsers = function (args) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getV2Response({
                        url: "/v2/UserManagement.svc/NotifyUsers",
                        data: args,
                        method: "post"
                    })];
            });
        }); };
        this.credentials = credentials;
        this.axiosClient = axios_1.default.create({
            baseURL: credentials.v2BaseUrl,
            headers: {
                "X-LSQ-Internal": credentials.authToken
            }
        });
        //request interceptor
        this.axiosClient.interceptors.request.use(function (config) {
            if (config === void 0) { config = {}; }
            var url = config.url;
            var params = "accessKey=" + credentials.accessKey + "&secretKey=" + credentials.secretKey;
            config.url = url + (url.indexOf("?") >= 0 ? "&" : "?") + params;
            return config;
        });
        this.authData = authData || {
            OrgShortCode: 0,
            Role: "",
            UserId: ""
        };
    }
    ApiV2BaseClient.getInstance = function (event, url, token, skipTransformError, timeout) {
        if (skipTransformError === void 0) { skipTransformError = false; }
        if (timeout === void 0) { timeout = 0; }
        var qs = event["queryStringParameters"];
        if (qs["accessKey"] && qs["secretKey"]) {
            ApiV2BaseClient.apiClient =
                ApiV2BaseClient.apiClient &&
                    ApiV2BaseClient.apiClient.credentials.accessKey === qs["accessKey"] &&
                    ApiV2BaseClient.apiClient.credentials.secretKey === qs["secretKey"]
                    ? ApiV2BaseClient.apiClient
                    : new ApiV2BaseClient({
                        accessKey: qs["accessKey"],
                        secretKey: qs["secretKey"],
                        authToken: token,
                        v2BaseUrl: url
                    });
            ApiV2BaseClient.apiClient.setSkipTransformError(skipTransformError);
            ApiV2BaseClient.apiClient.setTimeout(timeout);
            return ApiV2BaseClient.apiClient;
        }
        throw new Error("Missing Params: Required credentials are not passed!");
    };
    ApiV2BaseClient.cleanInstance = function () {
        ApiV2BaseClient.apiClient = null;
    };
    return ApiV2BaseClient;
}());
exports.default = ApiV2BaseClient;
