export default interface V2Credentials {
    accessKey: string;
    secretKey: string;
    v2BaseUrl: string;
    authToken?: string;
}
