"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken = require("jsonwebtoken");
var graphqlToJsonConverter = require("graphql-to-json-converter");
var AuthTokenSource_1 = require("./AuthTokenSource");
var AuthTokenBase = /** @class */ (function () {
    function AuthTokenBase() {
    }
    AuthTokenBase.retrieveCredentialsKeys = function (event, type, accessVariableName, secretVariableName) {
        if (accessVariableName === void 0) { accessVariableName = "accessKey"; }
        if (secretVariableName === void 0) { secretVariableName = "secretKey"; }
        var body = event.body && typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        var queryStringParams = event.queryStringParameters && typeof event.queryStringParameters === "string"
            ? JSON.parse(event.queryStringParameters)
            : event.queryStringParameters;
        var accessKey;
        var secretKey;
        switch (type) {
            case AuthTokenSource_1.AuthTokenSource.graphql:
                var jsonSchema = graphqlToJsonConverter(body.query);
                console.log("jsonSchema ----?", JSON.stringify(jsonSchema));
                var queryType = Object.keys(jsonSchema).find(function (x) { return x === "mutation" || x === "query"; });
                var queryValue = Object.keys(jsonSchema[queryType])[0];
                var accessKeyRawData = jsonSchema[queryType][queryValue]["args"][accessVariableName]["type"];
                accessKey = accessKeyRawData.endsWith(",")
                    ? accessKeyRawData.substring(1, accessKeyRawData.length - 2)
                    : accessKeyRawData.substring(1, accessKeyRawData.length - 1);
                console.log("accessKey", accessKey);
                var secretKeyRawData = jsonSchema[queryType][queryValue]["args"][secretVariableName]["type"];
                secretKey = secretKeyRawData.endsWith(",")
                    ? secretKeyRawData.substring(1, secretKeyRawData.length - 2)
                    : accessKeyRawData.substring(1, accessKeyRawData.length - 1);
                console.log("secretKey", secretKey);
                break;
            case AuthTokenSource_1.AuthTokenSource.restUrlWithBody:
                accessKey = body[accessVariableName];
                console.log("accessKey", accessKey);
                secretKey = body[secretVariableName];
                console.log("secretKey", secretKey);
                break;
            case AuthTokenSource_1.AuthTokenSource.restUrlWithQueryString:
                accessKey = queryStringParams[accessVariableName];
                console.log("accessKey", accessKey);
                secretKey = queryStringParams[secretVariableName];
                console.log("secretKey", secretKey);
                break;
            default:
                accessKey = "";
                secretKey = "";
                break;
        }
        var credentials = {
            accessKey: accessKey,
            secretKey: secretKey
        };
        return credentials;
    };
    AuthTokenBase.getEncryptedToken = function (authObj, accessKey, secretKey) {
        var secret = accessKey + secretKey;
        var token = jsonwebtoken.sign({ data: authObj }, secret, {
            algorithm: "HS512"
        });
        return token;
    };
    AuthTokenBase.getDecryptedToken = function (authToken, accessKey, secretKey) {
        try {
            var secret = accessKey + secretKey;
            var decryptData = jsonwebtoken.verify(authToken, secret);
            return decryptData.data;
        }
        catch (e) {
            return { statusCode: 500, Message: e.message };
        }
    };
    return AuthTokenBase;
}());
exports.default = AuthTokenBase;
