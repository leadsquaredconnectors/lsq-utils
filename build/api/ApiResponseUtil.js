"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiResponseUtil = /** @class */ (function () {
    function ApiResponseUtil() {
    }
    ApiResponseUtil.transformObjectArray = function (objArray, itemKeProperty, itemValueProperty) {
        return objArray.map(function (item) {
            return {
                key: item[itemKeProperty],
                value: item[itemValueProperty]
            };
        });
    };
    ApiResponseUtil.transformObjectCollection = function (list, itemKeProperty, itemValueProperty) {
        var newList = {};
        // eslint-disable-next-line
        Object.keys(list).map(function (key) {
            if (list[key].length > 0) {
                newList[key] = ApiResponseUtil.transformObjectArray(list[key], itemKeProperty, itemValueProperty);
            }
        });
        return newList;
    };
    ApiResponseUtil.getLeadFieldsByType = function (list, type, leadObject) {
        return list
            .filter(function (field) {
            return ((leadObject === undefined && field.DataType === type) ||
                (field.DataType === type &&
                    leadObject !== undefined &&
                    leadObject[field.SchemaName] !== null));
        })
            .map(function (field) {
            var item = leadObject !== undefined
                ? leadObject[field.SchemaName] + " (" + field.SchemaName + ")"
                : "Lead." + field.SchemaName;
            return {
                key: item,
                value: item
            };
        });
    };
    ApiResponseUtil.getReplacedValue = function (property, obj, key) {
        return obj[property] || "@{" + key + ":" + property + ",}";
    };
    ApiResponseUtil.getMailMergeFields = function (objectList, format, type) {
        var objects = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            objects[_i - 3] = arguments[_i];
        }
        var newFields = {};
        // eslint-disable-next-line
        Object.keys(objectList).map(function (key, index) {
            newFields[key] = objectList[key]
                .filter(function (field) {
                return (field.DisplayName === undefined || (type ? field.DataType === type : true));
            })
                .map(function (field) {
                if (field.DisplayName !== undefined) {
                    return {
                        schema: field.SchemaName,
                        key: field.DisplayName,
                        // eslint-disable-next-line
                        value: objects.length > index
                            ? ApiResponseUtil.getReplacedValue(field.SchemaName, objects[index], key)
                            : format.split("${").length === 3
                                ? format.replace("${0}", key).replace("${1}", field.SchemaName)
                                : format
                                    .replace("${0}", key)
                                    .replace("${1}", field.DisplayName)
                                    .replace(".${2}", "")
                    };
                }
                else {
                    return {
                        key: field.ActivityDisplayName,
                        value: field.FormMetaData.filter(function (field) {
                            return type ? field.DataType === type : true;
                        }).map(function (data) {
                            return {
                                schema: data.SchemaName,
                                key: data.DisplayName,
                                // eslint-disable-next-line
                                value: format.split("${").length === 3
                                    ? format
                                        .replace("${0}", key)
                                        .replace("${1}", data.SchemaName)
                                    : format
                                        .replace("${0}", key)
                                        .replace("${1}", field.ActivityDisplayName)
                                        .replace("${2}", data.DisplayName)
                            };
                        })
                    };
                }
            });
        });
        return newFields;
    };
    return ApiResponseUtil;
}());
exports.default = ApiResponseUtil;
