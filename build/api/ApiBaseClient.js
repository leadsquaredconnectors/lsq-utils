"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var ConsoleLogger_1 = __importDefault(require("../logger/ConsoleLogger"));
var exception_1 = require("../exception");
var ApiBaseClient = /** @class */ (function () {
    function ApiBaseClient(params, baseUrl) {
        var _this = this;
        this.makeGraphRequest = function (url, query) {
            var requestUrl = _this.getBaseUrl() + url;
            return new Promise(function (resolve, reject) {
                return _this.callApi()
                    .post(requestUrl, {
                    query: query
                })
                    .then(function (data) {
                    if (data && data.data.data) {
                        if (data.data.errors) {
                            //backend error
                            throw new exception_1.BackendError(JSON.stringify(data.data.errors), JSON.stringify({
                                query: query,
                                url: requestUrl
                            }));
                        }
                        else if (data.data.data) {
                            resolve(data.data.data);
                        }
                    }
                })
                    .catch(function (error) {
                    //const statusCode = error.response.status;
                    error.stack = JSON.stringify({
                        query: query,
                        url: requestUrl
                    });
                    reject(error);
                });
            });
        };
        this.logger = new ConsoleLogger_1.default(this.constructor.name);
        this.baseUrl = baseUrl || this.getBaseUrl();
        this.axiosClient = axios_1.default.create({
            baseURL: this.baseUrl
        });
        //set default error mechanism
        this.axiosClient.interceptors.response.use(function (response) {
            if (ApiBaseClient.checkIfLocalStorageEnabled() &&
                response.headers &&
                response.headers[ApiBaseClient.API_TOKEN_HEADER]) {
                window.localStorage.setItem(ApiBaseClient.API_TOKEN_HEADER, response.headers[ApiBaseClient.API_TOKEN_HEADER]);
            }
            return response;
        }, this.errorResponseHandler);
        if (params) {
            //set default params as param_$ header to identify while sending request
            var defaultParams_1 = {};
            Object.keys(params).forEach(function (key) {
                defaultParams_1["param_" + key] = params[key];
            });
            this.axiosClient.defaults.headers = defaultParams_1;
        }
        this.axiosClient.interceptors.request.use(function (config) {
            if (config === void 0) { config = {}; }
            //Reading default params as custom headers
            var params = [];
            Object.keys(config.headers).forEach(function (key) {
                if (key.split("_")[0] === "param") {
                    params.push(key.split("_")[1] + "=" + config.headers[key]);
                }
            });
            //Append params to url
            var url = config.url;
            config.url = url + (url.indexOf("?") >= 0 ? "&" : "?") + params.join("&");
            //delete custom headers
            Object.keys(config.headers).forEach(function (key) {
                if (key.split("_")[0] === "param") {
                    delete config.headers[key];
                }
            });
            //log request here
            //this.getLogger().info(url, config, {hello: true});
            if (ApiBaseClient.checkIfLocalStorageEnabled() &&
                window.localStorage.getItem(ApiBaseClient.API_TOKEN_HEADER)) {
                config.headers[ApiBaseClient.API_TOKEN_HEADER] = window.localStorage.getItem(ApiBaseClient.API_TOKEN_HEADER);
            }
            return config;
        });
    }
    ApiBaseClient.prototype.errorResponseHandler = function (error) {
        //log error here
        return Promise.reject(error);
    };
    ApiBaseClient.prototype.callApi = function () {
        return this.axiosClient;
    };
    /**
     *
     * @param url received from v2/ call to find the request host
     */
    ApiBaseClient.getUrlWithProtocol = function (url) {
        if (url && !url.startsWith("https://")) {
            return "https://" + url;
        }
        return url;
    };
    ApiBaseClient.prototype.getLogger = function () {
        return this.logger;
    };
    ApiBaseClient.API_TOKEN_HEADER = "x-api-key";
    ApiBaseClient.checkIfLocalStorageEnabled = function () {
        try {
            return window && window.localStorage ? true : false;
        }
        catch (e) {
            return false;
        }
    };
    return ApiBaseClient;
}());
exports.default = ApiBaseClient;
