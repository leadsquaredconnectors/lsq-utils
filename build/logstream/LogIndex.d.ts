export default interface LogIndex {
    orgId: number;
    providerId: number;
    requestId: string;
}
