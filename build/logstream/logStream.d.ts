import StreamClient from "aws-sdk/clients/kinesis";
import LSQLogger from "../logger";
import LogIndex from "./LogIndex";
export default class LogStream {
    logStreamName: string;
    streamClient: StreamClient;
    log: LSQLogger;
    source: string;
    index: {};
    constructor(config: any, index: LogIndex, streamClient: StreamClient);
    saveRecord(data: {}, docId?: string): Promise<unknown>;
    updateRecord(data: {}, docId: string): Promise<unknown>;
    static getInstance: (config: any, index: LogIndex) => Promise<LogStream | null>;
    private static getCrossAccountCredentials;
}
