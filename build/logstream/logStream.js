"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var awsSdk = __importStar(require("aws-sdk"));
var Logger_1 = require("../logger/Logger");
//process.env @param: LOG_STREAM_NAME,KINESIS_ROLE,LOG_STREAM_REGION
var LogStream = /** @class */ (function () {
    function LogStream(config, index, streamClient) {
        this.logStreamName = config.LOG_STREAM_NAME;
        this.streamClient = streamClient;
        this.log = Logger_1.getLogger(config);
        this.index = index;
        this.source = config.DATA_SOURCE;
    }
    LogStream.prototype.saveRecord = function (data, docId) {
        return __awaiter(this, void 0, void 0, function () {
            var documentId, record;
            var _this = this;
            return __generator(this, function (_a) {
                documentId = docId ? docId : uuid_1.v4();
                record = {
                    Data: JSON.stringify(Object.assign(data, {
                        doc_id: documentId,
                        source: this.source
                    }, this.index)),
                    StreamName: this.logStreamName,
                    PartitionKey: uuid_1.v4()
                };
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this.streamClient.putRecord(record, function (err, output) {
                            if (err) {
                                _this.log.error("Error in putRecord", {});
                                resolve({ status: "Failed", message: err.message });
                            }
                            _this.log.info("putRecord success", {});
                            resolve({ status: "Success", docId: documentId });
                        });
                    })];
            });
        });
    };
    LogStream.prototype.updateRecord = function (data, docId) {
        return __awaiter(this, void 0, void 0, function () {
            var record;
            var _this = this;
            return __generator(this, function (_a) {
                record = {
                    Data: JSON.stringify(Object.assign(data, {
                        doc_id: docId,
                        source: this.source
                    }, this.index)),
                    StreamName: this.logStreamName,
                    PartitionKey: uuid_1.v4()
                };
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this.streamClient.putRecord(record, function (err, output) {
                            if (err) {
                                _this.log.error("Error in Update Record", err);
                                resolve({ status: "Failed", message: err.message });
                            }
                            _this.log.info("Update Record success", {});
                            resolve({ status: "Success", docId: docId });
                        });
                    })];
            });
        });
    };
    LogStream.getCrossAccountCredentials = function (config) {
        return __awaiter(this, void 0, void 0, function () {
            var sts, params;
            return __generator(this, function (_a) {
                sts = new awsSdk.STS({ apiVersion: "2011-06-15" });
                params = {
                    RoleArn: config.KINESIS_ROLE,
                    RoleSessionName: "connectors"
                };
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        sts.assumeRole(params, function (err, data) {
                            if (err) {
                                console.log(err);
                                resolve(["error while assuming role", data]);
                            }
                            else {
                                resolve(["", data]);
                            }
                        });
                    })];
            });
        });
    };
    LogStream.getInstance = function (config, index) { return __awaiter(void 0, void 0, void 0, function () {
        var log, API_VERSION, response;
        var _a, _b, _c;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    log = Logger_1.getLogger(config);
                    API_VERSION = "2013-12-02";
                    if (!(config.KINESIS_ROLE && config.KINESIS_ROLE.length > 0)) return [3 /*break*/, 2];
                    return [4 /*yield*/, LogStream.getCrossAccountCredentials(config)];
                case 1:
                    response = _d.sent();
                    if (response[0]) {
                        return [2 /*return*/, null];
                    }
                    return [2 /*return*/, new LogStream(config, index, new awsSdk.Kinesis({
                            apiVersion: API_VERSION,
                            region: config.LOG_STREAM_REGION,
                            accessKeyId: (_a = response[1].Credentials) === null || _a === void 0 ? void 0 : _a.AccessKeyId,
                            secretAccessKey: (_b = response[1].Credentials) === null || _b === void 0 ? void 0 : _b.SecretAccessKey,
                            sessionToken: (_c = response[1].Credentials) === null || _c === void 0 ? void 0 : _c.SessionToken
                        }))];
                case 2:
                    log.info("initialize directly", {});
                    return [2 /*return*/, new LogStream(config, index, new awsSdk.Kinesis({
                            apiVersion: API_VERSION,
                            region: config.LOG_STREAM_REGION
                        }))];
            }
        });
    }); };
    return LogStream;
}());
exports.default = LogStream;
