import LSQLogger from "../logger";
import LogIndex from "./LogIndex";
export default class LogSearch {
    client: any;
    log: LSQLogger;
    source: string;
    logIndex: {};
    constructor(config: any, client: any, index: LogIndex);
    fetchRecords(query: object): Promise<any>;
    partialUpdate: (docID: string, data: object) => Promise<unknown>;
    static getInstance: (config: any, logIndex: LogIndex) => LogSearch;
}
