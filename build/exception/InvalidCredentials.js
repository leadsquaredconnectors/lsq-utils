"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidCredentials = void 0;
var ErrorType_1 = require("./ErrorType");
var InvalidCredentials = /** @class */ (function (_super) {
    __extends(InvalidCredentials, _super);
    function InvalidCredentials(msg) {
        var _this = _super.call(this, msg) || this;
        _this.name = "InvalidCredentials";
        _this.errorType = ErrorType_1.ErrorType.InvalidCredentials;
        _this.message = msg || "AccessKey OR SecretKey is Invalid.";
        _this.errorIcon = "icon-invalid";
        return _this;
    }
    return InvalidCredentials;
}(Error));
exports.InvalidCredentials = InvalidCredentials;
