export declare enum ErrorType {
    InvalidParams = 0,
    InvalidCredentials = 1,
    EmptyCredentials = 2,
    LeadOptedOutForSMS = 3,
    InvalidConfig = 4,
    FromNumberNotFound = 5,
    LeadDetailsNotFound = 6,
    BackendError = 7,
    LeadNumberNotFound = 8,
    ConnectorDisabled = 9,
    PermissionDenied = 10
}
