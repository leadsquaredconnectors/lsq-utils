"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BackendError = void 0;
var ErrorType_1 = require("./ErrorType");
var BackendError = /** @class */ (function (_super) {
    __extends(BackendError, _super);
    function BackendError(msg, query) {
        var _this = _super.call(this, msg) || this;
        _this.name = "BackendError";
        _this.errorType = ErrorType_1.ErrorType.BackendError;
        _this.message = msg;
        _this.stack = query;
        return _this;
    }
    return BackendError;
}(Error));
exports.BackendError = BackendError;
