import IError from "./IError";
import { ErrorType } from "./ErrorType";
export declare class PermissionDenied extends Error implements IError {
    errorType: ErrorType;
    errorIcon: string;
    constructor(msg?: string);
}
