"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeadDetailsNotFound = void 0;
var ErrorType_1 = require("./ErrorType");
var LeadDetailsNotFound = /** @class */ (function (_super) {
    __extends(LeadDetailsNotFound, _super);
    function LeadDetailsNotFound(msg) {
        var _this = _super.call(this, msg) || this;
        _this.name = "LeadDetailsNotFound";
        _this.errorType = ErrorType_1.ErrorType.LeadDetailsNotFound;
        _this.message = msg || "Failed to retrieve lead details. Please close and try again!";
        _this.errorIcon = "icon-missing";
        return _this;
    }
    return LeadDetailsNotFound;
}(Error));
exports.LeadDetailsNotFound = LeadDetailsNotFound;
