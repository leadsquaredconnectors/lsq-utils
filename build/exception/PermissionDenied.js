"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionDenied = void 0;
var ErrorType_1 = require("./ErrorType");
var PermissionDenied = /** @class */ (function (_super) {
    __extends(PermissionDenied, _super);
    function PermissionDenied(msg) {
        var _this = _super.call(this, msg) || this;
        _this.name = "PermissionDenied";
        _this.errorType = ErrorType_1.ErrorType.PermissionDenied;
        _this.message = msg || "You don't have permission to access this page. Please contact admin";
        _this.errorIcon = "icon-permission-denied";
        return _this;
    }
    return PermissionDenied;
}(Error));
exports.PermissionDenied = PermissionDenied;
