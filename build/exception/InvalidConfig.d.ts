import { ErrorType } from "./ErrorType";
import IError from "./IError";
export declare class InvalidConfig extends Error implements IError {
    errorType: ErrorType;
    errorIcon: string;
    constructor(msg?: string);
}
