"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeadNumberNotFound = void 0;
var ErrorType_1 = require("./ErrorType");
var LeadNumberNotFound = /** @class */ (function (_super) {
    __extends(LeadNumberNotFound, _super);
    function LeadNumberNotFound(msg) {
        var _this = _super.call(this, msg) || this;
        _this.name = "LeadNumberNotFound";
        _this.errorType = ErrorType_1.ErrorType.LeadNumberNotFound;
        _this.message = msg || "Failed to retrieve lead number. Please add lead phone number!";
        _this.errorIcon = "icon-missing";
        return _this;
    }
    return LeadNumberNotFound;
}(Error));
exports.LeadNumberNotFound = LeadNumberNotFound;
