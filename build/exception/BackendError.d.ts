import { ErrorType } from "./ErrorType";
import IError from "./IError";
export declare class BackendError extends Error implements IError {
    errorType: ErrorType;
    constructor(msg: string, query: string);
}
