"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidConfig = void 0;
var ErrorType_1 = require("./ErrorType");
var InvalidConfig = /** @class */ (function (_super) {
    __extends(InvalidConfig, _super);
    function InvalidConfig(msg) {
        var _this = _super.call(this, msg) || this;
        _this.name = "InvalidConfig";
        _this.errorType = ErrorType_1.ErrorType.InvalidConfig;
        _this.message = msg || "Connector is not configured yet. Please configure it by navigating to Apps & Marketplace with valid credentials.";
        _this.errorIcon = "icon-broken";
        return _this;
    }
    return InvalidConfig;
}(Error));
exports.InvalidConfig = InvalidConfig;
