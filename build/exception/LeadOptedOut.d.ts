import IError from "./IError";
import { ErrorType } from "./ErrorType";
export declare class LeadOptedOut extends Error implements IError {
    errorType: ErrorType;
    errorIcon: string;
    constructor(msg?: string);
}
