import IError from "./IError";
import { ErrorType } from "./ErrorType";
export declare class LeadDetailsNotFound extends Error implements IError {
    errorType: ErrorType;
    errorIcon: string;
    constructor(msg?: string);
}
