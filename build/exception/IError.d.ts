import { ErrorType } from "./ErrorType";
export default interface IError {
    name: string;
    errorType: ErrorType;
    message: string;
    errorIcon?: string;
}
