import { ErrorType } from "./ErrorType";
import IError from "./IError";
export declare class EmptyCredentials extends Error implements IError {
    errorType: ErrorType;
    errorIcon: string;
    constructor(msg?: string);
}
