"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeadOptedOut = void 0;
var ErrorType_1 = require("./ErrorType");
var LeadOptedOut = /** @class */ (function (_super) {
    __extends(LeadOptedOut, _super);
    function LeadOptedOut(msg) {
        var _this = _super.call(this, msg) || this;
        _this.name = "LeadOptedOut";
        _this.errorType = ErrorType_1.ErrorType.LeadOptedOutForSMS;
        _this.message = msg || "You can’t send message. This person has opted out.";
        _this.errorIcon = "icon-opted-out";
        return _this;
    }
    return LeadOptedOut;
}(Error));
exports.LeadOptedOut = LeadOptedOut;
