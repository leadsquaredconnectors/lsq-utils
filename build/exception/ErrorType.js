"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorType = void 0;
var ErrorType;
(function (ErrorType) {
    ErrorType[ErrorType["InvalidParams"] = 0] = "InvalidParams";
    ErrorType[ErrorType["InvalidCredentials"] = 1] = "InvalidCredentials";
    ErrorType[ErrorType["EmptyCredentials"] = 2] = "EmptyCredentials";
    ErrorType[ErrorType["LeadOptedOutForSMS"] = 3] = "LeadOptedOutForSMS";
    ErrorType[ErrorType["InvalidConfig"] = 4] = "InvalidConfig";
    ErrorType[ErrorType["FromNumberNotFound"] = 5] = "FromNumberNotFound";
    ErrorType[ErrorType["LeadDetailsNotFound"] = 6] = "LeadDetailsNotFound";
    ErrorType[ErrorType["BackendError"] = 7] = "BackendError";
    ErrorType[ErrorType["LeadNumberNotFound"] = 8] = "LeadNumberNotFound";
    ErrorType[ErrorType["ConnectorDisabled"] = 9] = "ConnectorDisabled";
    ErrorType[ErrorType["PermissionDenied"] = 10] = "PermissionDenied";
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
