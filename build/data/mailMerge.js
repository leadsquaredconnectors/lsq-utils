"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.populateTemplateKeyMapping = void 0;
exports.populateTemplateKeyMapping = function (Message, leadDetails, activityDetails, userDetails, taskDetails, linkDetails) {
    var activityData = activityDetails ? activityDetails.Data ? activityDetails.Data : activityDetails : activityDetails;
    while (Message.match(/@{(.*?),}/) !== null) {
        var value = Message.match(/@{(.*?),}/)[1];
        var data = value.toString().split(":");
        switch (data[0].toLowerCase()) {
            case 'lead':
                Message = mailMergeMessage(leadDetails, Message, data, value);
                break;
            case 'user':
                Message = mailMergeMessage(userDetails, Message, data, value);
                break;
            case 'activity':
                Message = mailMergeMessage(activityData, Message, data, value);
                break;
            case 'task':
                Message = mailMergeMessage(taskDetails, Message, data, value);
                break;
            case 'link':
                Message = mailMergeMessage(linkDetails, Message, data, value);
                break;
            default:
                Message = mailMergeMessage(null, Message, data, value);
        }
    }
    return Message;
};
var mailMergeMessage = function (details, Message, data, value) {
    var startIndex = Message.indexOf("@{");
    var endIndex = startIndex + value.length + 4;
    if (details && details.length) {
        var dataValue = details.filter(function (t) { return t.schema === data[1]; });
        if (dataValue.length > 0 && dataValue[0]['value'] && !dataValue[0]['value'].toString().startsWith("@{")) {
            return Message.substr(0, startIndex) + dataValue[0]['value'] + Message.substr(endIndex, Message.length);
        }
    }
    return Message.substr(0, startIndex) + Message.substr(endIndex, Message.length);
};
