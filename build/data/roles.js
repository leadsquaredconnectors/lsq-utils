"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = void 0;
exports.Roles = {
    adminRoles: [
        {
            value: "Administrator",
            label: "Administrator"
        },
        {
            value: "Marketing_User",
            label: "Marketing User"
        },
        {
            value: "Sales_Manager",
            label: "Sales Manager"
        },
        {
            value: "Sales_User",
            label: "Sales User"
        }
    ],
    managerRoles: [
        {
            value: "Sales_Manager",
            label: "Sales Manager"
        },
        {
            value: "Sales_User",
            label: "Sales User"
        }
    ],
    marketingRoles: [
        {
            value: "Marketing_User",
            label: "Marketing User"
        }
    ],
    salesRoles: [
        {
            value: "Sales_User",
            label: "Sales User"
        }
    ]
};
