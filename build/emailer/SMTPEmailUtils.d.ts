import EmailMessage from "./EmailMessage";
export default class SMTPEmailUtils {
    private transporter;
    constructor(config: {
        username: string;
        password: string;
        host: string;
        port?: number;
    });
    sendEmail: (emailMessage: EmailMessage) => Promise<any>;
}
