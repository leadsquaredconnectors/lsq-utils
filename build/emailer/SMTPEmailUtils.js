"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var nodemailer_1 = __importDefault(require("nodemailer"));
var SMTPEmailUtils = /** @class */ (function () {
    function SMTPEmailUtils(config) {
        var _this = this;
        this.sendEmail = function (emailMessage) {
            if (!emailMessage.to || !emailMessage.from || !emailMessage.subject) {
                throw new Error("To, From and Subject are mandatory fields");
            }
            return new Promise(function (resolve, reject) {
                _this.transporter
                    .sendMail(emailMessage)
                    .then(function (data) {
                    resolve({
                        accepted: data.accepted,
                        envelope: data.envelope,
                        messageId: data.messageId,
                        rejected: data.rejected,
                        messageSize: data.messageSize,
                    });
                })
                    .catch(function (err) {
                    reject(err);
                });
            });
        };
        this.transporter = nodemailer_1.default.createTransport({
            host: config.host,
            port: config.port || 587,
            secure: false,
            auth: {
                user: config.username,
                pass: config.password,
            },
        });
    }
    return SMTPEmailUtils;
}());
exports.default = SMTPEmailUtils;
