export default interface EmailMessage {
    from: string;
    to: string;
    subject: string;
    text?: string;
    html?: any;
    cc?: any;
    bcc?: any;
    attachments?: [{
        filename?: string;
        content?: any;
        path?: string;
        contentType?: string;
        encoding?: string;
    }];
}
