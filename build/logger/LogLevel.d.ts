export declare enum LogLevel {
    INFO = "info",
    ERROR = "error",
    FATAL = "fatal",
    WARN = "warn",
    DEBUG = "debug"
}
