export interface LogInterface {
    info(message: String, index: {}, ...data: any[]): void;
    error(message: String, index: {}, ...data: any[]): void;
    warn(message: String, index: {}, ...data: any[]): void;
    debug(message: String, index: {}, ...data: any[]): void;
}
