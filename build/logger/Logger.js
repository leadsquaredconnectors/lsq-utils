"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLogger = void 0;
var ConsoleLogger_1 = __importDefault(require("./ConsoleLogger"));
var LogglyLogger_1 = __importDefault(require("./LogglyLogger"));
exports.getLogger = function (config) {
    var logClassName = config.LOG_CLASS || "";
    var log = null;
    if ("" + config.LOG_TYPE === 'CONSOLE') {
        log = new ConsoleLogger_1.default(logClassName);
    }
    else {
        log = new LogglyLogger_1.default({
            token: "" + config.LOGGLY_TOKEN,
            subdomain: "" + config.LOGGLY_SUBDOMAIN,
            tags: ["" + config.LOGGLY_TAG]
        }, logClassName);
    }
    if (config.LOG_LEVEL) {
        log.setLogLevel(config.LOG_LEVEL.split(",") || []);
    }
    return log;
};
