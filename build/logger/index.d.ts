import { LogInterface } from "./LogInterface";
export default abstract class LSQLogger implements LogInterface {
    private className;
    private logLevel;
    constructor(className?: string);
    info(message: string, index: {}, ...data: any[]): void;
    error(message: string, index: {}, ...data: any[]): void;
    warn(message: string, index: {}, ...data: any[]): void;
    debug(message: string, index: {}, ...data: any[]): void;
    fatal(message: string, index: {}, ...data: any[]): void;
    protected abstract sendMessage(msgType: string, msg: string, index: {}, data: any[]): void;
    private cleanUpLog;
    protected emitLogMessage(msgType: string, msg: string, index: {}, data: any[]): void;
    setLogLevel: (levels: string[]) => void;
}
