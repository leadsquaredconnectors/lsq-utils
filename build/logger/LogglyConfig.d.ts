export default interface LogglyConfig {
    token: string;
    subdomain: string;
    tags: string[];
}
