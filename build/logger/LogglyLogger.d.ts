import LSQLogger from ".";
import LogglyConfig from "./LogglyConfig";
import { LogLevel } from "./LogLevel";
export default class LogglyLogger extends LSQLogger {
    _loggerObj: any;
    constructor(config: LogglyConfig, className?: string);
    protected sendMessage: (msgType: LogLevel.INFO | LogLevel.WARN | LogLevel.FATAL | LogLevel.ERROR, msg: string, index: {}, data: any[]) => void;
}
