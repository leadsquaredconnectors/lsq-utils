"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var LogLevel_1 = require("./LogLevel");
var LSQLogger = /** @class */ (function () {
    function LSQLogger(className) {
        var _this = this;
        this.cleanUpLog = function (logObject) {
            if (logObject) {
                if (logObject.secretKey) {
                    delete logObject.secretKey;
                }
                if (logObject.accessKey) {
                    delete logObject.accessKey;
                }
            }
            return logObject;
        };
        this.setLogLevel = function (levels) {
            levels.map(function (level) {
                _this.logLevel.push(LogLevel_1.LogLevel[level.toUpperCase()]);
            });
        };
        this.className = className || "";
        this.logLevel = [];
    }
    LSQLogger.prototype.info = function (message, index) {
        var data = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            data[_i - 2] = arguments[_i];
        }
        this.emitLogMessage(LogLevel_1.LogLevel.INFO, message, index, data);
    };
    LSQLogger.prototype.error = function (message, index) {
        var data = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            data[_i - 2] = arguments[_i];
        }
        this.emitLogMessage(LogLevel_1.LogLevel.ERROR, message, index, data);
    };
    LSQLogger.prototype.warn = function (message, index) {
        var data = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            data[_i - 2] = arguments[_i];
        }
        this.emitLogMessage(LogLevel_1.LogLevel.WARN, message, index, data);
    };
    LSQLogger.prototype.debug = function (message, index) {
        var data = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            data[_i - 2] = arguments[_i];
        }
        this.emitLogMessage(LogLevel_1.LogLevel.DEBUG, message, index, data);
    };
    LSQLogger.prototype.fatal = function (message, index) {
        var data = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            data[_i - 2] = arguments[_i];
        }
        this.emitLogMessage(LogLevel_1.LogLevel.FATAL, "FATAL: " + message, index, data);
    };
    LSQLogger.prototype.emitLogMessage = function (msgType, msg, index, data) {
        var copyIndex = __assign({}, index);
        if (this.className) {
            msg = this.className + ":" + msg;
        }
        if (copyIndex) {
            copyIndex = this.cleanUpLog(copyIndex);
        }
        if (this.logLevel.length === 0 ||
            this.logLevel.indexOf(LogLevel_1.LogLevel[msgType.toUpperCase()]) >= 0) {
            this.sendMessage(msgType, msg, copyIndex, data);
        }
    };
    return LSQLogger;
}());
exports.default = LSQLogger;
