"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var _1 = __importDefault(require("."));
var LogLevel_1 = require("./LogLevel");
var winston = require("winston");
var Loggly = require("winston-loggly-bulk").Loggly;
var LogglyLogger = /** @class */ (function (_super) {
    __extends(LogglyLogger, _super);
    function LogglyLogger(config, className) {
        var _this = _super.call(this, className) || this;
        _this.sendMessage = function (msgType, msg, index, data) {
            if (msgType === LogLevel_1.LogLevel.FATAL) {
                winston.log(LogLevel_1.LogLevel.ERROR, msg, data);
            }
            else {
                winston.log(msgType, msg, index, data.length ? data : []);
            }
        };
        _this._loggerObj = new Loggly(__assign(__assign({}, config), { json: true }));
        winston.add(_this._loggerObj);
        return _this;
    }
    return LogglyLogger;
}(_1.default));
exports.default = LogglyLogger;
