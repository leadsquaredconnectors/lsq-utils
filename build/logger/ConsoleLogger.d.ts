import LSQLogger from ".";
import { LogLevel } from "./LogLevel";
export default class ConsoleLogger extends LSQLogger {
    constructor(className?: string);
    protected sendMessage: (msgType: LogLevel, msg: string, index: {}, data: any[]) => void;
}
