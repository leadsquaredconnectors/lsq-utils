"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var _1 = __importDefault(require("."));
var LogLevel_1 = require("./LogLevel");
var ConsoleLogger = /** @class */ (function (_super) {
    __extends(ConsoleLogger, _super);
    function ConsoleLogger(className) {
        var _this = _super.call(this, className) || this;
        _this.sendMessage = function (msgType, msg, index, data) {
            if (msgType === LogLevel_1.LogLevel.FATAL) {
                msgType = LogLevel_1.LogLevel.ERROR;
            }
            console[msgType](msg, index, data);
        };
        return _this;
    }
    return ConsoleLogger;
}(_1.default));
exports.default = ConsoleLogger;
