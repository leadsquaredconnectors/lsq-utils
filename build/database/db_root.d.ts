import dynamoose from 'dynamoose';
export declare class DBRoot {
    tableSchemaData: any;
    DBSchemas: any;
    DBInitData: any;
    constructor(schema?: any, data?: any);
    createDynamooseInstance(accessKey: string, secretKey: string, region: string): void;
    bootsrap(accessKeyId: string, secretAccessKey: string, region: string): Promise<void>;
    getModel(): dynamoose.ModelConstructor<unknown, unknown>;
    importTables(): Promise<any>;
    saveTable(): Promise<any>;
    initTableSave(): Promise<any>;
}
