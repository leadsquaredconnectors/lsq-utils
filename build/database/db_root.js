"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DBRoot = void 0;
var dynamoose_1 = __importDefault(require("dynamoose"));
var DBRoot = /** @class */ (function () {
    function DBRoot(schema, data) {
        this.tableSchemaData = {};
        this.DBSchemas = schema;
        this.DBInitData = data;
    }
    DBRoot.prototype.createDynamooseInstance = function (accessKey, secretKey, region) {
        dynamoose_1.default.AWS.config.update({
            accessKeyId: accessKey,
            secretAccessKey: secretKey,
            region: region
        });
    };
    ;
    DBRoot.prototype.bootsrap = function (accessKeyId, secretAccessKey, region) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.createDynamooseInstance(accessKeyId, secretAccessKey, region);
                return [2 /*return*/];
            });
        });
    };
    ;
    DBRoot.prototype.getModel = function () {
        var dbSchema = this.DBSchemas;
        var schema = new dynamoose_1.default.Schema(dbSchema['Schema'], dbSchema['SchemaOptions']);
        var model = null;
        if (Object.keys(dbSchema['Options']).length > 0) {
            model = dynamoose_1.default.model(dbSchema['TableName'], schema, dbSchema['Options']);
        }
        else {
            model = dynamoose_1.default.model(dbSchema['TableName'], schema);
        }
        return model;
    };
    DBRoot.prototype.importTables = function () {
        return __awaiter(this, void 0, void 0, function () {
            var tableSchemas_1;
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    tableSchemas_1 = JSON.parse(JSON.stringify(this.tableSchemaData));
                    this.DBSchemas.forEach(function (ele) { return __awaiter(_this, void 0, void 0, function () {
                        var schema;
                        return __generator(this, function (_a) {
                            schema = new dynamoose_1.default.Schema(ele.Schema, ele.SchemaOptions);
                            if (Object.keys(ele.Options).length > 0) {
                                tableSchemas_1[ele.TableName] = dynamoose_1.default.model(ele.TableName, schema, ele.Options);
                            }
                            else {
                                tableSchemas_1[ele.TableName] = dynamoose_1.default.model(ele.TableName, schema);
                            }
                            return [2 /*return*/];
                        });
                    }); });
                    this.tableSchemaData = tableSchemas_1;
                }
                catch (e) {
                    return [2 /*return*/, { Status: "Failed", Message: JSON.stringify(e) }];
                }
                return [2 /*return*/];
            });
        });
    };
    DBRoot.prototype.saveTable = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    if (this.DBInitData.length > 0) {
                        this.DBInitData.forEach(function (ele) { return __awaiter(_this, void 0, void 0, function () {
                            var TableName, schemas, schema, model, Data;
                            var _this = this;
                            return __generator(this, function (_a) {
                                TableName = ele['TableName'];
                                schemas = this.DBSchemas.find(function (x) { return x['TableName'] === TableName; });
                                schema = new dynamoose_1.default.Schema(schemas.Schema, schemas.SchemaOptions);
                                if (Object.keys(schemas.Options).length > 0) {
                                    model = dynamoose_1.default.model(schemas.TableName, schema, schemas.Options);
                                }
                                else {
                                    model = dynamoose_1.default.model(schemas.TableName, schema);
                                }
                                Data = ele['Data'];
                                Data.forEach(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                    var dataInstance;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                dataInstance = new model(e);
                                                return [4 /*yield*/, dataInstance.save()];
                                            case 1:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                                return [2 /*return*/];
                            });
                        }); });
                    }
                }
                catch (e) {
                    return [2 /*return*/, { Status: "Failed", Message: JSON.stringify(e) }];
                }
                return [2 /*return*/];
            });
        });
    };
    DBRoot.prototype.initTableSave = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    if (this.DBInitData.length > 0) {
                        this.DBInitData.forEach(function (ele) { return __awaiter(_this, void 0, void 0, function () {
                            var TableName, Data;
                            var _this = this;
                            return __generator(this, function (_a) {
                                TableName = ele['TableName'];
                                Data = ele['Data'];
                                Data.forEach(function (e) { return __awaiter(_this, void 0, void 0, function () {
                                    var dataInstance;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                console.log("e", e);
                                                dataInstance = new this.tableSchemaData[TableName](e);
                                                return [4 /*yield*/, dataInstance.save()];
                                            case 1:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                                return [2 /*return*/];
                            });
                        }); });
                    }
                }
                catch (e) {
                    return [2 /*return*/, { Status: "Failed", Message: JSON.stringify(e) }];
                }
                return [2 /*return*/];
            });
        });
    };
    return DBRoot;
}());
exports.DBRoot = DBRoot;
